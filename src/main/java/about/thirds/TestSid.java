package about.thirds;

import lombok.extern.slf4j.Slf4j;
import org.n3r.idworker.Sid;

/**
 * 雪花id生成器
 */
@Slf4j
public class TestSid {
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            final String shortId = Sid.nextShort();
            log.info("the shortId: {}",shortId);
        }

        for (int i = 0; i < 10; i++) {
            final String longId = Sid.next();
            log.info("the shortId: {}",longId);
        }
    }
}
