package about.cgtest;

import net.sf.cglib.core.DebuggingClassWriter;
import net.sf.cglib.proxy.Enhancer;

public class TestIt {
    public static void main(String[] args) {
        System.setProperty(DebuggingClassWriter.DEBUG_LOCATION_PROPERTY, "/Users/cqg/about/about-algorithm");
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(MyService.class);
        enhancer.setCallback(new MyCallBack());
        MyService o = (MyService)enhancer.create();
        o.name();
        o.age();
        o.go();
    }
}
