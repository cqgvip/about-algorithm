package about.cgtest.mp;

import net.sf.cglib.core.Signature;

import java.util.Arrays;

public class SigTest {
    public static void main(String[] args) {
        Signature abc = new Signature("abc", "()LString");
        System.out.println(abc.getName());
        System.out.println(abc.getReturnType());
        System.out.println(Arrays.toString(abc.getArgumentTypes()));

    }
}
