package about.cgtest.mp;

import about.cgtest.MyService;
import net.sf.cglib.proxy.MethodProxy;

public class MpTest {
    public static void main(String[] args) throws Throwable {
        MethodProxy methodProxy = MethodProxy
                .create(MyService.class, MyServiceSub.class
                        , "()Ljava/lang/String;"
                        , "name", "name222");
        System.out.println(methodProxy.getSuperIndex());
        System.out.println(methodProxy.getSuperName());
        System.out.println("===========");
        MyServiceSub myServiceSub = new MyServiceSub();
        Object b = methodProxy.invoke(myServiceSub, new Object[]{});
        System.out.println(b);
        System.out.println("===========");
        Object aaa = methodProxy.invokeSuper(myServiceSub, new Object[]{});
        System.out.println(aaa);

    }
}
