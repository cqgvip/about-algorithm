package about.cgtest.mp;

import about.cgtest.MyService;

public class MyServiceSub extends MyService {

    public String name222() {
        System.out.println("before name");
        return super.name();
    }

    public int age222() {
        System.out.println("before age");
        return super.age();
    }
}
