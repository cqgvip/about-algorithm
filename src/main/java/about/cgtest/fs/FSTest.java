package about.cgtest.fs;

import net.sf.cglib.core.DebuggingClassWriter;
import net.sf.cglib.reflect.FastClass;

import java.lang.reflect.InvocationTargetException;

public class FSTest {
    public static void main(String[] args) throws InvocationTargetException {

        System.setProperty(DebuggingClassWriter.DEBUG_LOCATION_PROPERTY, "/Users/cqg/about/about-algorithm");
        FastClass fc = FastClass.create(MyFunc.class);
        int index = fc.getIndex(new Class[]{});
        System.out.println(index);
        MyFunc myFunc = (MyFunc) fc.newInstance(0, new Object[]{});
        myFunc.doIt(1);
        int age = fc.getIndex("age", new Class[]{int.class});
        System.out.println(age);
        int age2 = fc.getIndex("age", new Class[]{long.class});
        System.out.println(age2);
    }
}
