package about.cgtest.fs;

public class MyFunc {

    public void doIt(){
        System.out.println("doit");
    }
    public void doIt(int a){
        System.out.println("doit");
    }

    public void doIt(int a,int b){
        System.out.println("doit");
    }

    public void doIt(long a,int b){
        System.out.println("doit");
    }

    public void hello(Integer v){
        System.out.println("hello");
    }

    public void hello(Number v){
        System.out.println("hello");
    }

    public void hello(Long v){
        System.out.println("hello");
    }

    public void age(int age){
        System.out.println("this is age");
    }
}
