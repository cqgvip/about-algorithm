package about.cgtest;

public class MyService {

    public String name(){
        System.out.println("name is abc");
        return "abc";
    }

    public int age(){
        int age=30;
        System.out.println("age is : " + age);
        return age;
    }

    public final void go(){
        System.out.println("this is final go");
    }
}
