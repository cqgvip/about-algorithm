package about.tree;

import lombok.Data;

@Data
public class Node {
    private int val;
    private Node left;
    private Node right;

    public Node(int val) {
        this.val = val;
    }
}
