package about.self;

import com.hankcs.hanlp.HanLP;
import com.hankcs.hanlp.seg.common.Term;
import com.huaban.analysis.jieba.JiebaSegmenter;

import java.util.*;
import java.util.function.Function;

/**
 * 计算字符串相似度
 */
public class SimilarStr {

    public static void main(String[] args) {
        String str1="abdefg";
        String str2="acdefg";
        System.out.println(method1(str1,str2));
        final String chStr = "你吃饭了吗?";
        final List<Term> list = HanLP.segment(chStr);
        System.out.println(list);
        System.out.println(HanLP.convertToPinyinList(chStr));
    }

    /**
     * 余弦相似度
     * @return
     */
    public static double method1(String str1,String str2) {
        if(str1==null) str1="";
        if(str2==null) str2="";
        if(str1.equals(str2)){
            return 1;
        }
        if("".equals(str1)||"".equals(str2)){
            return 0;
        }
        Function<String,Map<Character,Integer>> mapFunc=str->{
            Map<Character,Integer> map=new HashMap<>();
            for (char c : str.toCharArray()) {
                Integer num = map.get(c);
                num=num==null?1:num+1;
                map.put(c,num);
            }
            return map;
        };
        Set<Character> set=new HashSet<>();
        final Map<Character, Integer> map1 = mapFunc.apply(str1);
        final Map<Character, Integer> map2 = mapFunc.apply(str2);
        set.addAll(map1.keySet());
        set.addAll(map2.keySet());
        double dx,d1,d2;
        dx=d1=d2=0;
        for (Character c : set) {
            final Integer v1 = map1.getOrDefault(c, 0);
            final Integer v2 = map2.getOrDefault(c, 0);
            dx+=v1*v2;
            d1+=v1*v1;
            d2+=v2*v2;
        }

        return dx/(Math.sqrt(d1)*Math.sqrt(d2));
    }


    public static void testDemo() {
        JiebaSegmenter segmenter = new JiebaSegmenter();
        String[] sentences =
                new String[] {"这是一个伸手不见五指的黑夜。我叫孙悟空，我爱北京，我爱Python和C++。", "我不喜欢日本和服。", "雷猴回归人间。",
                        "工信处女干事每月经过下属科室都要亲口交代24口交换机等技术性器件的安装工作", "结果婚的和尚未结过婚的"};
        for (String sentence : sentences) {
            System.out.println(segmenter.process(sentence, JiebaSegmenter.SegMode.INDEX).toString());
        }
    }
}
