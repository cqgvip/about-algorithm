package about.self;

import lombok.extern.slf4j.Slf4j;

/**
 * 十进制 转二进制 并格式化
 */
@Slf4j
public class Dec2BinStr {


    public static void main(String[] args) {
        print(1024);
        //dec 1024
        //bin 100 0000 0000
    }

    public static void print(long decVal){
        log.info("\ndec:{}\nbin:{}",decVal,dec2binStr(decVal));
    }

    public static String dec2binStr(long decVal){
        //正则左右预查锁定位置
        final String binStr = Long.toBinaryString(decVal).replaceAll("(?=((\\d{4})+$)(?<=\\d))", " ");
        return binStr;
    }
}
