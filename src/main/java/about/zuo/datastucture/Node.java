package about.zuo.datastucture;

import lombok.Data;

/**
 * 单链表逆序
 */
@Data
public class Node {
    private int val;
    private Node next;

    public Node(int val) {
        this.val = val;
    }

    @Override
    public String toString() {
        return "val=" + val +
                ", next->" + next;
    }
}
