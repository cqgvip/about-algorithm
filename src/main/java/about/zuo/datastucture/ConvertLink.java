package about.zuo.datastucture;


/**
 * 单链表逆序
 */
public class ConvertLink {

    public static void main(String[] args) {
        Node head = demoData();
        Node head2 = convert(head);
        System.out.println(head2);

        System.out.println(convert(null));

    }


    public static Node convert(Node head){
        Node pre = null;
        Node next = null;
        while (head!=null){
            next = head.getNext();
            head.setNext(pre);
            pre = head;
            head = next;
        }
        return pre;
    }

    public static Node demoData(){
        Node node1 = new Node(1);
        Node node2 = new Node(2);
        Node node3 = new Node(3);
        Node node4 = new Node(4);
        node1.setNext(node2);
        node2.setNext(node3);
        node3.setNext(node4);
        Node head = node1;
        System.out.println(head);
        return head;
    }
}
