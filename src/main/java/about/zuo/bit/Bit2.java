package about.zuo.bit;


import java.util.Arrays;

/**
 * 一组数据 有两个数 分别出现一次，另外有多个数 分别出现2次
 * 请找出这个两个数 是 谁
 */
public class Bit2 {

    public static void main(String[] args) {
        int[] arr = {1, 2, 2, 3, 3, 5, 5, 7, 9, 9};
        int[] ans = resolve(arr);
        System.out.println(Arrays.toString(arr));
        System.out.println(Arrays.toString(ans));
    }


    public static int[] resolve(int[] arr){

        //求出a^b
        int xor=0;
        for (int val : arr) {
            xor ^= val;
        }

        //区分a b 的特征
        int groupBy = xor ^ (-xor);

        //计算a值
        int a=0;
        for (int val : arr) {
            if((val&xor)!=0){
                a ^= val;
            }
        }

        //计算b值
        int b=xor^a;

        return new int[]{a, b};
    }





}
