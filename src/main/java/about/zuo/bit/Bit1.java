package about.zuo.bit;


/**
 * 有一个数出现一次，其它数出现两次 ，请找出这个数
 */
public class Bit1 {

    public static void main(String[] args) {
        int[] arr = demoData();
        if(arr==null||arr.length==0){
            throw new RuntimeException("数组长度必须大于0");
        }
        int resolve = resolve(arr);
        System.out.println(resolve);
    }

    public static int resolve(int[] arr){
        int xor=0;
        for (int v : arr) {
            xor^=v;
        }
        return xor;
    }

    public static int[] demoData(){
        int[] arr = {2, 2, 3, 5, 5, 9, 9};
        return arr;
    }
}
