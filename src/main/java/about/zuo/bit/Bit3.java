package about.zuo.bit;

/**
 * 有一个数出现了k次 ，其它数出现了m次，k>0 m>0 切 k!=m
 * 请找出这个数
 */
public class Bit3 {

    public static void main(String[] args) {
        int[] arr = {2, 2, 3, 3, 3, 5, 5, 5, 8, 8, 8, 2, 7};
        int ans = resolve(arr, 2, 3);
        System.out.println(ans);
    }

    public static int resolve(int[] arr,int k,int m){
        //按位做统计
        int[] statInfos=new int[32];
        for (int val : arr) {
            for (int i = 0; i < 32; i++) {
                statInfos[i] += (val >> i) & 1;
            }
        }

        //计算结果值
        int ans=0;
        for (int i = 0; i < 32; i++) {
            if (statInfos[i] % m != 0) {
                ans |= 1 << i;
            }
        }

        return ans;
    }
}
