package about.zuo.sort.guibing;

import java.util.Arrays;

public class SmallSumGuibing {

    public static void main(String[] args) {
        int[] arr = demoArr(100, 30);
        System.out.println(Arrays.toString(arr));
        int rightAns = sum2(arr);
        System.out.println(rightAns);
        int myAns = sum1(arr);
        System.out.println(myAns);
        System.out.println(rightAns==myAns);
    }

    //方式1 计算小和
    public static int sum1(int[] arr){
        if(arr==null||arr.length<1){
            return 0;
        }
        return sum1(arr, 0, arr.length-1);
    }

    public static int sum1(int[] arr,int l,int r){
        if(l==r){
            return 0;
        }
        int mid=l+((r-l)>>1);
        int ans=0;
        ans += sum1(arr, l, mid);
        ans += sum1(arr, mid + 1, r);
        ans += merge(arr, l, mid, r);
        return ans;
    }

    public static int merge(int[] arr,int l,int m,int r){
        int[] tmps = new int[r - l + 1];
        int lf=l;
        int rf=m+1;
        int i=0;
        int ans=0;
        while (lf<=m&&rf<=r){
            if(arr[lf]<arr[rf]){
                ans += arr[lf] * (r - rf + 1);
                tmps[i++] = arr[lf++];
            } else {
                tmps[i++] = arr[rf++];
            }
        }
        while (lf<=m){
            tmps[i++] = arr[lf++];
        }
        while (rf<=r){
            tmps[i++] = arr[rf++];
        }
        for (i = 0; i < tmps.length; i++) {
            arr[l + i] = tmps[i];
        }
        return ans;
    }


    //方式2 计算小和
    public static int sum2(int[] arr){
        int ans = 0;
        for (int i = 1; i < arr.length; i++) {
            for (int j = 0; j < i; j++) {
                ans += arr[j] < arr[i] ? arr[j] : 0;
            }
        }
        return ans;
    }


    public static int[] demoArr(int maxVal,int randomLen){
        int len =(int)( Math.random() * randomLen)+1;
        int[] arr = new int[len];
        for (int i = 0; i <len; i++) {
            arr[i]=(int)( Math.random() * maxVal)+1;
        }
        return arr;
    }
}
