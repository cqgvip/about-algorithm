package about.zuo.sort.guibing;


import java.util.Arrays;


/**
 * 求逆序对的数目
 * 归并逆排序   求解
 */
public class PairNumGuibing2 {

    public static void main(String[] args) {
        //获取样本数据
        int[] arr = demoArr(100, 40);


        //打印原始数组
        System.out.println(Arrays.toString(arr));
        //打印正确的值
        int rightCount = rightCount(arr);
        System.out.println("rightNum:" + rightCount);
        System.out.println(Arrays.toString(arr));
        //打印我计算的值
        int myCount = myCount(arr, 0, arr.length - 1);
        System.out.println("myNum:" + myCount);
        System.out.println(Arrays.toString(arr));
        //判断结果是否正确
        System.out.println("isOk:" + (rightCount == myCount));
    }

    //比对器计算值 O(N平方)
    public static int rightCount(int[] arr){
        int ans=0;
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i+1; j < arr.length; j++) {
                if(arr[i]>arr[j]){
                    ans++;
                }
            }
        }
        return ans;
    }

    //我的计算值 O(N*logN)
    public static int myCount(int[] arr,int l,int r){
        if(arr==null||arr.length<1){
            return 0;
        }
        if(l==r){
            return 0;
        }
        int ans=0;
        int mid = l + ((r - l) >> 1);
//        System.out.println(Arrays.toString(arr));
//        System.out.println("l:"+l+" r:"+r);
        ans += myCount(arr, l, mid);
//        System.out.println("left:"+ans);
//        System.out.println(Arrays.toString(arr));
        ans += myCount(arr, mid + 1, r);
        //merge data
        ans += merge(arr, l, mid, r);
        return ans;
    }

    //todo merge data
    public static int merge(int[] arr,int l,int m,int r){

        int ans = 0;
        int[] tmps = new int[r - l + 1];
        int lf=l;
        int rf=m+1;
        int i=0;
        while (lf<=m&&rf<=r){
            if(arr[lf]>arr[rf]){
                tmps[i++] = arr[lf++];
                ans += r - rf + 1;
            } else {
                tmps[i++] = arr[rf++];
            }
        }
        while (lf<=m){
            tmps[i++] = arr[lf++];
        }
        while (rf<=r){
            tmps[i++] = arr[rf++];
        }

        for (i = 0; i < tmps.length; i++) {
            arr[l+i]=tmps[i];
        }
        return ans;
    }


    //获取样本数据
    public static int[] demoArr(int maxVal,int randomLen){
        int len =(int)( Math.random() * randomLen)+1;
        int[] arr = new int[len];
        for (int i = 0; i <len; i++) {
            arr[i]=(int)( Math.random() * maxVal)+1;
        }
        return arr;
    }

}
