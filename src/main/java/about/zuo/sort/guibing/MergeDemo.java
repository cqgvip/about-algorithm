package about.zuo.sort.guibing;

import java.util.Arrays;

public class MergeDemo {
    public static void main(String[] args) {
        int[] arr1={1,8,10,12,14};
        int[] arr2={2,5,8,10};
        int[] arr = merge(arr1, arr2);
        System.out.println(Arrays.toString(arr1));
        System.out.println(Arrays.toString(arr2));
        System.out.println(Arrays.toString(arr));
    }

    public static int[] merge(int[] arr1,int[] arr2){
        if(arr1==null){
            return arr2;
        }
        if(arr2==null){
            return arr1;
        }
        int l=0;
        int r=0;
        int i=0;
        int[] arr=new int[arr1.length + arr2.length];
        while (l<arr1.length&&r<arr2.length){
            arr[i++] = arr1[l] <= arr2[r] ? arr1[l++] : arr2[r++];
        }
        while (l<arr1.length){
            arr[i++] = arr1[l++];
        }
        while (r<arr2.length){
            arr[i++] = arr2[r++];
        }
        return arr;

    }
}
