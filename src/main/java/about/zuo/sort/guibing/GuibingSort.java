package about.zuo.sort.guibing;

import java.util.Arrays;

public class GuibingSort {


    public static void main(String[] args) {
        int[] arr = {1, 5, 4, 2, 10, 1, 3};
        System.out.println(Arrays.toString(arr));
        sort(arr,0,arr.length-1);
        System.out.println(Arrays.toString(arr));

    }

    public static void sort(int[] arr,int l,int r){
        if(arr==null||l==r){
            return;
        }
        int mid=l+((r-l)>>1);
        sort(arr, l, mid);
        sort(arr, mid + 1, r);
        merge(arr,l,mid,r);
    }

    public static void merge(int[] arr,int l,int m,int r){
        int[] tmps = new int[r - l + 1];
        int lf=l;
        int rf=m+1;
        int i=0;
        while (lf<=m&&rf<=r){
            tmps[i++] = arr[lf] <= arr[rf] ? arr[lf++] : arr[rf++];
        }
        while (lf<=m){
            tmps[i++] = arr[lf++];
        }
        while (rf<=r){
            tmps[i++] = arr[rf++];
        }
        for (i = 0; i < tmps.length; i++) {
            arr[l+i]=tmps[i];
        }
    }
}
