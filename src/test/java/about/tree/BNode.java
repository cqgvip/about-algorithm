package about.tree;

import lombok.Data;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

@Data
public class BNode {
    private int v;
    private BNode left;
    private BNode right;

    public static BNode of(int v) {
        final BNode n = new BNode();
        n.setV(v);
        return n;

    }


    BNode root=null;
    @Before
    public void beforeClass() throws Exception {
//              1
//           2     3
//        4    5
//       6       7
//        8        9
        BNode[] ns=new BNode[10];
        for (int i = 1; i <= 9; i++) {
            ns[i]=of(i);
        }
        ns[1].setLeft(ns[2]);
        ns[1].setRight(ns[3]);
        ns[2].setLeft(ns[4]);
        ns[2].setRight(ns[5]);
        ns[4].setLeft(ns[6]);
        ns[5].setRight(ns[7]);
        ns[6].setRight(ns[8]);
        ns[7].setRight(ns[9]);
        root=ns[1];
    }

    @Test
    public void name() {
        System.out.println(root);
    }

    @Override
    public String toString() {
        return "BNode{" +
                "v=" + v +
                '}';
    }

    @Test
    public void testOrder(){
        System.out.println("pre "+preOrder(root,new ArrayList<>()));
        System.out.println("in "+ inOrder(root,new ArrayList<>()));
        System.out.println("back "+backOrder(root,new ArrayList<>()));
    }

    private List<BNode> preOrder(BNode root, List<BNode> list){
        if(root!=null){
            list.add(root);
            preOrder(root.left,list);
            preOrder(root.right,list);
        }
        return list;
    }

    private List<BNode> inOrder(BNode root, List<BNode> list){
        if(root!=null){
            inOrder(root.left,list);
            list.add(root);
            inOrder(root.right,list);
        }
        return list;
    }

    private List<BNode> backOrder(BNode root, List<BNode> list){
        if(root!=null){
            backOrder(root.left,list);
            backOrder(root.right,list);
            list.add(root);
        }
        return list;
    }


}
