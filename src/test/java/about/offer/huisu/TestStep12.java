package about.offer.huisu;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

@Slf4j
public class TestStep12 {

    @Test
    public void test(){
        char[] cs=new char[]{
                'a','b','t','g',
                'c','f','c','s',
                'j','d','e','h'
        };
        final boolean result = hasStep(cs, 3, 4, "bfcsh", true);
        final boolean result2 = hasStep(cs, 3, 4, "abfb", true);

    }

    public boolean hasStep(char[] cs,int rows,int cols,String str,boolean printStep){
        if(cs==null||str==null||rows<1||cols<1){
            return false;
        }
        if(cs.length!=rows*cols){
            throw new RuntimeException("length not march rows*cols");
        }
        boolean[] flags=new boolean[rows*cols];

        boolean lastResult=false;
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                lastResult=doStep(cs,str,rows,cols,row,col,0,flags);
                if(lastResult){
                    break;
                }
            }
            if(lastResult){
                break;
            }
        }
        if(printStep){
            // 打印路径
            if(lastResult){
                System.out.println("has paths");
                final StringBuffer title = new StringBuffer("\t");
                for (int i = 0; i < cols; i++) {
                    title.append(i).append("\t");
                }
                System.out.println(title);
                for (int row = 0; row < rows; row++) {
                    StringBuffer sb=new StringBuffer().append(row).append("\t");
                    for (int col = 0; col < cols; col++) {
                        if(flags[row*cols+col]){
                            sb.append(cs[row*cols+col]);
                        } else {
                            sb.append('*');
                        }
                        sb.append("\t");
                    }
                    System.out.println(sb);
                }
            } else {
                System.out.println("not has steps");
            }

        }
        return lastResult;


    }

    public boolean doStep(char[] cs, String str, int rows, int cols, int row, int col, int index, boolean[] flags) {
        if (index == str.length()) {
            return true;
        }
        final int pos = row * cols + col;
        boolean result=false;
        if(col>=0&&col<cols&&row>=0&&row<=rows&&!flags[pos]&&cs[pos]==str.charAt(index)){
            flags[pos]=true;
            result=doStep(cs,str,rows,cols,row+1,col,index+1,flags)  //up
                ||doStep(cs,str,rows,cols,row-1,col,index+1,flags)   //down
                ||doStep(cs,str,rows,cols,row,col+1,index+1,flags)   //right
                ||doStep(cs,str,rows,cols,row,col-1,index+1,flags);  //left
            if(!result){
                flags[pos]=false;
            }
        }
        return result;
    }
}
