package about.offer.huisu;

import org.junit.Test;

public class TestMax13 {

    @Test
    public void test(){
        System.out.println("result:"+maxCount(15,32,32,true));
    }
    public int maxCount(int num,int rows,int cols,boolean print){
        if(num<1||rows<1||cols<1){
            return 0;
        }
        boolean[] flags=new boolean[rows*cols];
        final int result = doMaxCount(num, rows, cols, 0, 0, flags);
        if(print){
            final StringBuffer title = new StringBuffer("\t");
            for (int i = 0; i < cols; i++) {
                title.append(i);
                title.append("\t");
            }
            System.out.println(title);
            for (int i = 0; i < rows; i++) {
                final StringBuffer rowStr = new StringBuffer(i+"\t");
                for (int j = 0; j < cols; j++) {
                    rowStr.append(flags[i*cols+j]?"o":"x");
                    rowStr.append("\t");
                }
                System.out.println(rowStr);
            }
        }
        return result;
    }

    public int doMaxCount(int num,int rows,int cols,int row,int col,boolean[] flags){
        //todo 递归计算
        int count=0;
        if(canStep(num,rows,cols,row,col,flags)){
            flags[row*cols+col]=true;
            count+=1;
            count+=doMaxCount(num,rows,cols,row+1,col,flags);
            count+=doMaxCount(num,rows,cols,row-1,col,flags);
            count+=doMaxCount(num,rows,cols,row,col+1,flags);
            count+=doMaxCount(num,rows,cols,row,col-1,flags);
        }
        return count;
    }

    public boolean canStep(int num,int rows,int cols,int row,int col,boolean[] flags){
        //todo 判断某一点是否可走
        if(row>=0&&row<rows){
            if(col>=0&&col<cols){
                try{
                    if(!flags[row*cols+col]){
                        //todo cmp(row+col,num)
                        if(convVal(row)+convVal(col)<num){
                            return true;
                        }
                    }
                }catch (Exception e){
                    System.out.println("row:"+row+",col:"+col);
                    throw new RuntimeException(e);
                }

            }
        }
        return false;
    }

    public int convVal(int val){
        int sum=0;
        while (val>0){
            sum+=val%10;
            val/=10;
        }
        return sum;
    }
}
