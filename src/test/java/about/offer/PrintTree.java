package about.offer;

import apple.laf.JRSUIUtils;
import lombok.Data;
import org.junit.Test;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

public class PrintTree {
    @Data
    public static class TreeNode{
        private int value;
        private TreeNode left;
        private TreeNode right;
    }

    @Test
    public void test(){
        /**
         *        1
         *    2       3
         *  4  5     6
         *       7    8
         */
        TreeNode[] arr=new TreeNode[9];
        for (int i = 1; i <=8; i++) {
            final TreeNode node = new TreeNode();
            node.value=i;
            arr[i]=node;
        }
        arr[1].left=arr[2];
        arr[1].right=arr[3];
        arr[2].left=arr[4];
        arr[2].right=arr[5];
        arr[3].left=arr[6];
        arr[5].right=arr[7];
        arr[6].right=arr[8];
        printTree(arr[1]);
    }

    public void printTree(TreeNode root){
        Queue<TreeNode> queue=new LinkedList<>();
        if(root==null){
            return;
        }
        queue.offer(root);
        TreeNode node;
        while ((node = queue.poll())!=null){
            System.out.print(node.value+" ");
            final TreeNode left = node.getLeft();
            final TreeNode right = node.getRight();
            if(left!=null){
                queue.offer(left);
            }
            if(right!=null){
                queue.offer(right);
            }
        }
        System.out.println("\nover");
    }
}
